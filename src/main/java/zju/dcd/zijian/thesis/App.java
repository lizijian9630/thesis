package zju.dcd.zijian.thesis;

import java.io.IOException;

import zju.dcd.zijian.thesis.experiment.Experiment7;

public class App {
	public static void main(String[] args) throws IOException {	

		Experiment7.process("E:/ThesisWorkspace/expdata/1",
				"E:/ThesisWorkspace/result/1", 6);
		Experiment7.process("E:/ThesisWorkspace/expdata/2",
				"E:/ThesisWorkspace/result/2", 6);
		Experiment7.process("E:/ThesisWorkspace/expdata/sift",
				"E:/ThesisWorkspace/result/sift", 25);
		Experiment7.process("E:/ThesisWorkspace/expdata/hog",
				"E:/ThesisWorkspace/result/hog", 25);

	}
}
