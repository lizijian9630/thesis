package zju.dcd.zijian.thesis.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.deeplearning4j.word2vec.tokenizer.DefaultTokenizerFactory;
import org.deeplearning4j.word2vec.tokenizer.Tokenizer;
import org.deeplearning4j.word2vec.tokenizer.TokenizerFactory;

import zju.dcd.zijian.thesis.domain.Sentence;

public class TopSentenceSelector {
	private static TokenizerFactory tokenizerFactory = new DefaultTokenizerFactory();

	public static List<Entry<Sentence, Double>> select(int nWords,
			List<Entry<Sentence, Double>> input) {

		List<Entry<Sentence, Double>> result = new ArrayList<>();

		for (Entry<Sentence, Double> entry : input) {
			Tokenizer tokenizer = tokenizerFactory.create(entry.getKey().text);
			if (tokenizer.countTokens() <= nWords) {
				result.add(entry);
				nWords -= tokenizer.countTokens();
			}
		}

		return result;
	}
}
