package zju.dcd.zijian.thesis.regulation.scorer;

import zju.dcd.zijian.thesis.domain.Sentence;

public class PositionScorer implements BaseRegulationScorer {
	public double calculateScore(Sentence sentence) {
		return 1 - (double) sentence.position / sentence.totalSentenceNumber;
	}
}
