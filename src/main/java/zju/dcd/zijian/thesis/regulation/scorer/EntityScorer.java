package zju.dcd.zijian.thesis.regulation.scorer;

import java.util.Hashtable;

import zju.dcd.zijian.thesis.domain.Entity;
import zju.dcd.zijian.thesis.domain.Sentence;

public class EntityScorer implements BaseRegulationScorer {

	// <Type, <Text, Weight>>
	public Hashtable<String, Hashtable<String, Double>> ImportantEntities;

	public EntityScorer(
			Hashtable<String, Hashtable<String, Double>> ImportantEntities) {
		this.ImportantEntities = ImportantEntities;
	}

	public EntityScorer() {
		this.ImportantEntities = null;
	}

	public double calculateScore(Sentence sentence) {
		double result = 0;
		for (Entity entity : sentence.entities) {
			result += entity.relevance * entity.count;
			if (ImportantEntities != null
					&& ImportantEntities.containsKey(entity.type)
					&& ImportantEntities.get(entity.type).containsKey(
							entity.text)) {
				result += ImportantEntities.get(entity.type).get(entity.text);
			}
		}
		return result;
	}
}
