package zju.dcd.zijian.thesis.regulation.scorer;

import zju.dcd.zijian.thesis.domain.Concept;
import zju.dcd.zijian.thesis.domain.Sentence;

public class ConceptScorer implements BaseRegulationScorer {
	public double calculateScore(Sentence sentence) {
		double result = 0;
		for (Concept concept : sentence.concepts) {
			result += concept.relevance;
		}
		return result;
	}
}
