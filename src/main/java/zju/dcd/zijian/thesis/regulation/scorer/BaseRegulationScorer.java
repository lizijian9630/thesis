package zju.dcd.zijian.thesis.regulation.scorer;

import zju.dcd.zijian.thesis.domain.Sentence;

public interface BaseRegulationScorer {
	public double calculateScore(Sentence sentence);
}
