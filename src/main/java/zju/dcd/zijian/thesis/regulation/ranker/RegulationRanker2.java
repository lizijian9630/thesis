package zju.dcd.zijian.thesis.regulation.ranker;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import zju.dcd.zijian.thesis.domain.Document;
import zju.dcd.zijian.thesis.domain.Sentence;

// Baseline Coverage Ranker
// The first sentence of each document has score 1
// Other sentences has score 0
public class RegulationRanker2 {
	public List<Entry<Sentence, Double>> rankSentences(List<Document> docList) {

		List<Entry<Sentence, Double>> result = new ArrayList<Entry<Sentence, Double>>();

		for (Document doc : docList) {
			Boolean isFirst = true;
			for (Sentence sentence : doc.sentences) {
				if (isFirst) {
					result.add(new AbstractMap.SimpleEntry<Sentence, Double>(
							sentence, 1.0));
					isFirst = false;
				}
			}
		}

		return result;
	}
}
