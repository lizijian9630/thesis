package zju.dcd.zijian.thesis.regulation.scorer;

import zju.dcd.zijian.thesis.domain.Sentence;

public class LengthScorer implements BaseRegulationScorer {
	public int sentenceMinLength;

	public LengthScorer(int sentenceMinLength) {
		this.sentenceMinLength = sentenceMinLength;
	}

	public LengthScorer() {
		this(10);
	}

	public double calculateScore(Sentence sentence) {
		return sentence.text.split(" ").length < this.sentenceMinLength ? 0 : 1;
	}
}
