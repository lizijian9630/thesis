package zju.dcd.zijian.thesis.regulation.ranker;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.List;
import java.util.Map.Entry;

import zju.dcd.zijian.thesis.domain.Document;
import zju.dcd.zijian.thesis.domain.Sentence;
import zju.dcd.zijian.thesis.regulation.scorer.BaseRegulationScorer;

// Weighted Sum of Regulation Scores
public class RegulationRanker1 {

	public List<Entry<Sentence, Double>> rankSentences(List<Document> docList,
			Hashtable<BaseRegulationScorer, Double> scorerList) {

		List<Entry<Sentence, Double>> result = new ArrayList<Entry<Sentence, Double>>();

		for (Document doc : docList) {
			for (Sentence sentence : doc.sentences) {
				double score = 0;
				for (Entry<BaseRegulationScorer, Double> entry : scorerList
						.entrySet()) {
					score += entry.getKey().calculateScore(sentence)
							* entry.getValue();
				}
				result.add(new AbstractMap.SimpleEntry<Sentence, Double>(
						sentence, score));
			}
		}

		Collections.sort(result, new Comparator<Entry<Sentence, Double>>() {
			public int compare(Entry<Sentence, Double> e1,
					Entry<Sentence, Double> e2) {
				return e1.getValue().equals(e2.getValue()) ? 0
						: e1.getValue() > e2.getValue() ? -1 : 1;
			}
		});

		return result;
	}
}
