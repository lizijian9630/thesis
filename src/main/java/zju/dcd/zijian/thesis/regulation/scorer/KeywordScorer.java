package zju.dcd.zijian.thesis.regulation.scorer;

import zju.dcd.zijian.thesis.domain.Keyword;
import zju.dcd.zijian.thesis.domain.Sentence;

public class KeywordScorer implements BaseRegulationScorer {
	public double calculateScore(Sentence sentence) {
		double result = 0;
		for (Keyword keyword : sentence.keywords) {
			result += keyword.relevance;
		}
		return result;
	}

}
