package zju.dcd.zijian.thesis.domain;

import java.util.List;

public class Document {
	public String topic;
	public String title;
	public List<Sentence> sentences;
	public List<Taxonomy> taxonomies;
}
