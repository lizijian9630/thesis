package zju.dcd.zijian.thesis.domain;

import java.util.List;

public class Sentence {
	public int position;
	public int totalSentenceNumber;
	public String text;
	public List<Keyword> keywords;
	public List<Concept> concepts;
	public List<Entity> entities;
}
