package zju.dcd.zijian.thesis.dataset.tac;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.InputSource;

import zju.dcd.zijian.thesis.domain.Concept;
import zju.dcd.zijian.thesis.domain.Document;
import zju.dcd.zijian.thesis.domain.Entity;
import zju.dcd.zijian.thesis.domain.Keyword;
import zju.dcd.zijian.thesis.domain.Sentence;
import zju.dcd.zijian.thesis.domain.Taxonomy;

public final class TacDocBuilder {

	private static SAXBuilder saxBuilder = new SAXBuilder();

	private static String readAllText(String docURI) throws IOException {
		InputStream in = new FileInputStream(new File(docURI));
		byte inputBuffer[] = new byte[in.available()];
		in.read(inputBuffer);
		in.close();
		return new String(inputBuffer);
	}

	private static List<Taxonomy> buildTaxonomies(String docURI)
			throws JSONException, IOException {
		List<Taxonomy> taxonomies = new ArrayList<Taxonomy>();
		JSONArray taxonomiesJSON = new JSONObject(readAllText(docURI
				+ ".Taxonomy")).getJSONArray("taxonomy");
		for (int i = 0; i < taxonomiesJSON.length(); ++i) {
			JSONObject taxonomyJSON = taxonomiesJSON.getJSONObject(i);
			Taxonomy taxonomy = new Taxonomy();
			taxonomy.label = taxonomyJSON.getString("label").trim();
			taxonomy.score = taxonomyJSON.getDouble("score");
			taxonomy.confident = !"no".equals(taxonomyJSON.optString(
					"confident", null));
			taxonomies.add(taxonomy);
		}
		return taxonomies;
	}

	private static String cleanString(String src) {
		String dst = src.replace('\n', ' ').replaceAll("[^a-zA-Z]", " ");
		return dst;
	}

	private static List<Sentence> buildSentences(String docURI,
			List<Element> textElement) throws JSONException, IOException {
		List<Sentence> sentences = new ArrayList<Sentence>();

		for (int i = 0; i < textElement.size(); ++i) {
			Sentence sentence = new Sentence();
			sentence.text = cleanString(textElement.get(i).getTextTrim());
			sentence.position = i + 1;
			sentence.totalSentenceNumber = textElement.size();
			sentence.keywords = buildKeywords(docURI, sentence.text);
			sentence.concepts = buildConcepts(docURI, sentence.text);
			sentence.entities = buildEntities(docURI, sentence.text);

			sentences.add(sentence);
		}

		return sentences;
	}

	private static List<Keyword> buildKeywords(String docURI,
			String sentenceText) throws JSONException, IOException {
		List<Keyword> keywords = new ArrayList<Keyword>();
		JSONArray keywordsJSON = new JSONObject(readAllText(docURI
				+ ".Keywords")).getJSONArray("keywords");
		for (int i = 0; i < keywordsJSON.length(); ++i) {
			JSONObject keywordJSON = keywordsJSON.getJSONObject(i);
			String keywordText = keywordJSON.getString("text").trim();
			if (sentenceText.contains(keywordText)) {
				Keyword keyword = new Keyword();
				keyword.text = keywordText;
				keyword.relevance = keywordJSON.getDouble("relevance");
				keywords.add(keyword);
			}
		}
		return keywords;
	}

	private static List<Concept> buildConcepts(String docURI,
			String sentenceText) throws JSONException, IOException {
		List<Concept> concepts = new ArrayList<Concept>();
		JSONArray conceptsJSON = new JSONObject(readAllText(docURI
				+ ".Concepts")).getJSONArray("concepts");
		for (int i = 0; i < conceptsJSON.length(); ++i) {
			JSONObject conceptJSON = conceptsJSON.getJSONObject(i);
			String conceptText = conceptJSON.getString("text").trim();
			if (sentenceText.contains(conceptText)) {
				Concept concept = new Concept();
				concept.text = conceptText;
				concept.relevance = conceptJSON.getDouble("relevance");
				concepts.add(concept);
			}
		}
		return concepts;
	}

	private static List<Entity> buildEntities(String docURI, String sentenceText)
			throws JSONException, IOException {
		List<Entity> entities = new ArrayList<Entity>();
		JSONArray entitiesJSON = new JSONObject(readAllText(docURI
				+ ".NamedEntities")).getJSONArray("entities");
		for (int i = 0; i < entitiesJSON.length(); ++i) {
			JSONObject entityJSON = entitiesJSON.getJSONObject(i);
			String entityText = entityJSON.getString("text").trim();
			if (sentenceText.contains(entityText)) {
				Entity entity = new Entity();
				entity.text = entityText;
				entity.type = entityJSON.getString("type").trim();
				entity.relevance = entityJSON.getDouble("relevance");
				entity.count = entityJSON.getInt("count");
				entities.add(entity);
			}
		}
		return entities;
	}

	public static Document buildSingleDocument(String topic, String docURI)
			throws JDOMException, IOException, ParseException {

		org.jdom2.Document xmldoc = saxBuilder.build(new InputSource(
				new StringReader(readAllText(docURI).replace("&", "&amp;"))));
		Element rootElement = xmldoc.getRootElement();
		Element bodyElement = rootElement.getChild("BODY");
		if (bodyElement == null)
			bodyElement = rootElement;
		List<Element> textElement = bodyElement.getChild("TEXT").getChildren(
				"P");
		if (textElement == null || textElement.isEmpty()) {
			textElement = bodyElement.getChild("TEXT").getChildren("p");
		}

		Document doc = new Document();
		doc.topic = topic;
		doc.taxonomies = buildTaxonomies(docURI);
		doc.sentences = buildSentences(docURI, textElement);

		doc.title = null;
		Element headerElement = bodyElement.getChild("HEADLINE");
		if (headerElement == null)
			headerElement = rootElement.getChild("HEADER");
		if (headerElement != null)
			doc.title = headerElement.getTextTrim();

		return doc;
	}

	public static List<Document> buildDocumentSet(String rootURI, String setId,
			String setType) throws JDOMException, IOException, ParseException {
		String setRootPath = String.format("%1$s/%2$s/%2$s-%3$s", new File(
				rootURI).getAbsolutePath(), setId, setType);
		String filePaths[] = new File(setRootPath).list(new TacDocFilter());

		List<Document> docList = new ArrayList<Document>();
		for (String filePath : filePaths) {
			Document doc = TacDocBuilder.buildSingleDocument(
					TacTopicSearcher.getTopicName(setId),
					String.format("%s/%s", setRootPath, filePath));
			docList.add(doc);
		}

		return docList;
	}
}