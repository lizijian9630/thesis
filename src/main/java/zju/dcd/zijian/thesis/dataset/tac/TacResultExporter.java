package zju.dcd.zijian.thesis.dataset.tac;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.Hashtable;
import java.util.List;
import java.util.Map.Entry;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import zju.dcd.zijian.thesis.domain.Sentence;

public class TacResultExporter {
	private static void deleteDirectory(File path) {
		if (!path.exists())
			return;
		if (path.isFile()) {
			path.delete();
			return;
		}
		File[] files = path.listFiles();
		for (int i = 0; i < files.length; i++) {
			deleteDirectory(files[i]);
		}
		path.delete();
	}

	private static void copyFile(File s, File t) {
		FileInputStream fi = null;
		FileOutputStream fo = null;
		FileChannel in = null;
		FileChannel out = null;
		try {
			fi = new FileInputStream(s);
			fo = new FileOutputStream(t);
			in = fi.getChannel();
			out = fo.getChannel();
			in.transferTo(0, in.size(), out);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				fi.close();
				in.close();
				fo.close();
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void export(
			Hashtable<String, Hashtable<String, List<Entry<Sentence, Double>>>> result,
			String outputRootPath, String originModelPath,
			String remoteModelPath, String remoteResultPath) throws IOException {
		File rootDirectory = new File(outputRootPath);
		deleteDirectory(rootDirectory);
		rootDirectory.mkdirs();

		String rootDirectoryPath = rootDirectory.getAbsolutePath();
		File resultDirectory = new File(String.format("%s/results",
				rootDirectoryPath));
		File modelDirectory = new File(String.format("%s/models",
				rootDirectoryPath));
		resultDirectory.mkdir();
		modelDirectory.mkdir();

		for (File originModelFile : new File(originModelPath).listFiles()) {
			copyFile(
					originModelFile,
					new File(String.format("%s/%s",
							modelDirectory.getAbsolutePath(),
							originModelFile.getName())));
		}

		Document document = new Document();
		Element rootElement = new Element("ROUGE-EVAL");
		rootElement.setAttribute("version", "1.0");

		// <topic, method, result sentences>
		for (Entry<String, Hashtable<String, List<Entry<Sentence, Double>>>> resultEntry : result
				.entrySet()) {
			String topicId = resultEntry.getKey();
			Element peersElement = new Element("PEERS");
			for (Entry<String, List<Entry<Sentence, Double>>> topicEntry : resultEntry
					.getValue().entrySet()) {
				String methodId = topicEntry.getKey();
				String resultFilePath = String.format("%s/%s-%s",
						resultDirectory.getAbsolutePath(), topicId, methodId);
				BufferedWriter resultFileWriter = new BufferedWriter(
						new FileWriter(resultFilePath));
				for (Entry<Sentence, Double> sentenceEntry : topicEntry
						.getValue()) {
					resultFileWriter.write(String.format("%s%n",
							sentenceEntry.getKey().text));
				}
				resultFileWriter.close();
				Element singlePeerElement = new Element("P");
				singlePeerElement.setAttribute("ID", methodId);
				singlePeerElement.setText(String.format("%s-%s", topicId,
						methodId));
				peersElement.addContent(singlePeerElement);
			}

			Element modelsElement = new Element("MODELS");
			for (File modelFile : modelDirectory.listFiles()) {
				String modelFileName = modelFile.getName();
				if (modelFileName.startsWith(String.format("%s-A", topicId))) {
					Element singleModelElement = new Element("M");
					singleModelElement.setAttribute("ID",
							modelFileName.split("\\.")[4]);
					singleModelElement.setText(modelFileName);
					modelsElement.addContent(singleModelElement);
				}
			}

			Element peerRootElement = new Element("PEER-ROOT");
			peerRootElement.setText(remoteResultPath);
			Element modelRootElement = new Element("MODEL-ROOT");
			modelRootElement.setText(remoteModelPath);
			Element inputFormatElement = new Element("INPUT-FORMAT");
			inputFormatElement.setAttribute("TYPE", "SPL");

			Element evalElement = new Element("EVAL");
			evalElement.setAttribute("ID", topicId);
			evalElement.addContent(peerRootElement);
			evalElement.addContent(modelRootElement);
			evalElement.addContent(inputFormatElement);
			evalElement.addContent(peersElement);
			evalElement.addContent(modelsElement);

			rootElement.addContent(evalElement);
		}

		document.setRootElement(rootElement);

		XMLOutputter configXmlOutputter = new XMLOutputter(
				Format.getPrettyFormat());
		configXmlOutputter.output(
				document,
				new FileOutputStream(String.format("%s/config.xml",
						rootDirectoryPath)));
	}
}
