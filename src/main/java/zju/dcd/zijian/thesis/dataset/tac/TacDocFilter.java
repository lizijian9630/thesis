package zju.dcd.zijian.thesis.dataset.tac;

import java.io.File;
import java.io.FilenameFilter;

public class TacDocFilter implements FilenameFilter {

	public boolean accept(File dir, String name) {
		return name.indexOf('.') == name.lastIndexOf('.');
	}

}
