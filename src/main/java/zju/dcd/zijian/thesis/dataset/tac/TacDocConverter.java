package zju.dcd.zijian.thesis.dataset.tac;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;

import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import zju.dcd.zijian.thesis.domain.Document;
import zju.dcd.zijian.thesis.domain.Sentence;

public class TacDocConverter {
	private static void deleteDirectory(File path) {
		if (!path.exists())
			return;
		if (path.isFile()) {
			path.delete();
			return;
		}
		File[] files = path.listFiles();
		for (int i = 0; i < files.length; i++) {
			deleteDirectory(files[i]);
		}
		path.delete();
	}

	public static void convertToMeadFormat(String outputRootPath,
			String localDocumentPath, String remoteMeadDirectory, String method)
			throws JDOMException, IOException, ParseException {

		File outputRootDirectory = new File(outputRootPath);
		deleteDirectory(outputRootDirectory);
		outputRootDirectory.mkdirs();

		File outputSummaryDirectory = new File(outputRootPath + "/summary");
		outputSummaryDirectory.mkdir();

		FileWriter shellFileWriter = new FileWriter(String.format("%s/run.sh",
				outputRootPath));

		for (File docDirectory : new File(localDocumentPath).listFiles()) {
			String docDirectoryName = docDirectory.getName();

			String setRootPath = String
					.format("%1$s/%2$s/%2$s-%3$s",
							new File(localDocumentPath).getAbsolutePath(),
							docDirectoryName, "A");
			String fileNames[] = new File(setRootPath)
					.list(new TacDocFilter());

			File docSetDirectory = new File(String.format("%s/%s",
					outputRootPath, docDirectoryName));
			File docsentDirectory = new File(String.format("%s/%s/docsent",
					outputRootPath, docDirectoryName));
			docSetDirectory.mkdir();
			docsentDirectory.mkdir();

			XMLOutputter xmlOutputter = new XMLOutputter(
					Format.getPrettyFormat());

			org.jdom2.Document xmlDoc = new org.jdom2.Document();
			Element rootElement = new Element("CLUSTER");
			rootElement.setAttribute("LANG", "ENG");
			for (String fileName : fileNames) {
				Element docElement = new Element("D");
				docElement.setAttribute("DID", fileName);
				rootElement.addContent(docElement);

				Document doc = TacDocBuilder.buildSingleDocument(
						TacTopicSearcher.getTopicName(docDirectoryName),
						String.format("%s/%s", setRootPath, fileName));

				org.jdom2.Document xmlDocSent = new org.jdom2.Document();
				Element docSentRootElement = new Element("DOCSENT");
				docSentRootElement.setAttribute("DID", fileName);
				docSentRootElement.setAttribute("DOCNO", fileName);
				docSentRootElement.setAttribute("LANG", "ENG");

				Element docSentBodyElement = new Element("BODY");
				Element docSentTextElement = new Element("TEXT");

				int count = 1;
				for (Sentence sentence : doc.sentences) {
					Element docSentSentElement = new Element("S");
					docSentSentElement.setAttribute("PAR",
							Integer.toString(count));
					docSentSentElement.setAttribute("RSNT",
							Integer.toString(count));
					docSentSentElement.setAttribute("SNO",
							Integer.toString(count));
					docSentSentElement.setText(sentence.text);
					docSentTextElement.addContent(docSentSentElement);
					++count;
				}
				docSentBodyElement.addContent(docSentTextElement);
				docSentRootElement.addContent(docSentBodyElement);
				xmlDocSent.setRootElement(docSentRootElement);

				xmlOutputter.output(
						xmlDocSent,
						new FileOutputStream(String.format("%s/%s.docsent",
								docsentDirectory.getAbsolutePath(), fileName)));
			}
			xmlDoc.setRootElement(rootElement);

			xmlOutputter.output(
					xmlDoc,
					new FileOutputStream(String
							.format("%s/%s.cluster",
									docSetDirectory.getAbsolutePath(),
									docDirectoryName)));

			shellFileWriter
					.write(String
							.format("sudo perl %1$s/bin/mead.pl -w -a 100 -%4$s -o %1$s/data/%3$s/summary/%2$s.summary %1$s/data/%3$s/%2$s \n",
									remoteMeadDirectory, docDirectoryName,
									outputRootDirectory.getName(), method));

			System.out.println(docDirectoryName);
		}
		shellFileWriter.close();
	}
}
