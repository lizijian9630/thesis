package zju.dcd.zijian.thesis.dataset.tac;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.xml.sax.InputSource;

public class TacTopicFileConverter {
	private static String readAllText(String docURI) throws IOException {
		InputStream in = new FileInputStream(new File(docURI));
		byte inputBuffer[] = new byte[in.available()];
		in.read(inputBuffer);
		in.close();
		return new String(inputBuffer);
	}

	public static void convert(String inputPath, String outputPath)
			throws JDOMException, IOException {
		SAXBuilder saxBuilder = new SAXBuilder();
		Document xmldoc = saxBuilder.build(new InputSource(new StringReader(
				readAllText(inputPath).replace("&", "&amp;"))));
		Element rootElement = xmldoc.getRootElement();
		List<Element> topicElement = rootElement.getChildren("topic");

		BufferedWriter outFileWriter = new BufferedWriter(new FileWriter(
				outputPath));
		for (Element topic : topicElement) {
			String topicId = topic.getAttributeValue("id").trim();
			String topicTitle = topic.getChildTextTrim("title");
			outFileWriter.write(String.format("%s|%s%n", topicId, topicTitle));
		}
		outFileWriter.close();
	}
}
