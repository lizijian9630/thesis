package zju.dcd.zijian.thesis.dataset.tac;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Hashtable;

public class TacTopicSearcher {
	private static BufferedReader reader = null;
	private static Hashtable<String, String> topics = new Hashtable<String, String>();

	public static void setTopics(String topicFilePath) throws IOException {
		reader = new BufferedReader(new FileReader(topicFilePath));
		String line;
		while ((line = reader.readLine()) != null) {
			String topic[] = line.split("\\|");
			topics.put(topic[0], topic[1]);
		}
		reader.close();
	}

	public static String getTopicName(String topicId) {
		return topics.get(topicId);
	}
}
