package zju.dcd.zijian.thesis.deeplearning.utils;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map.Entry;

import zju.dcd.zijian.thesis.domain.Sentence;

public class SentenceVectorExporter {
	public static void export(List<Entry<Sentence, double[]>> sentenceVectors,
			String outputFilePath) throws IOException {

		FileWriter fw = new FileWriter(outputFilePath);

		for (Entry<Sentence, double[]> sv : sentenceVectors) {
			fw.write(sv.getKey().text + "\n");
			for (double vector : sv.getValue()) {
				fw.write(vector + " ");
			}
			fw.write("\n");
		}

		fw.close();
	}
}
