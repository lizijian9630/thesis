package zju.dcd.zijian.thesis.deeplearning.core.sent2vec;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Stack;

import org.deeplearning4j.berkeley.Triple;
import org.deeplearning4j.stopwords.StopWords;
import org.deeplearning4j.util.MatrixUtil;
import org.deeplearning4j.word2vec.VocabWord;
import org.deeplearning4j.word2vec.inputsanitation.InputHomogenization;
import org.deeplearning4j.word2vec.sentenceiterator.CollectionSentenceIterator;
import org.deeplearning4j.word2vec.sentenceiterator.SentenceIterator;
import org.deeplearning4j.word2vec.sentenceiterator.SentencePreProcessor;
import org.deeplearning4j.word2vec.tokenizer.DefaultTokenizerFactory;
import org.deeplearning4j.word2vec.tokenizer.Tokenizer;
import org.jblas.DoubleMatrix;
import org.jblas.util.Random;

public class Word2VecSingleThread {

	// Constants
	protected static final DefaultTokenizerFactory tokenizerFactory = new DefaultTokenizerFactory();
	protected final static List<String> stopWords = StopWords.getStopWords();

	// Params
	public int minWordFrequency;
	public int windowSize;
	public int layerSize;
	public double alpha; // learning rate

	// Vocabulary
	protected Map<String, VocabWord> vocab = new HashMap<>();

	// Input Layer
	protected DoubleMatrix syn0;
	// Hidden Layer
	protected DoubleMatrix syn1;

	public void train(Collection<String> sentences, String model, int times) {
		SentenceIterator sentenceIter = new CollectionSentenceIterator(
				new SentencePreProcessor() {
					@Override
					public String preProcess(String sentence) {
						return new InputHomogenization(sentence).transform();
					}
				}, sentences);

		this.buildVocab(sentenceIter);
		this.buildHuffmanTree();
		this.resetLayers();
		this.processSentences(sentenceIter, model, times);
	}

	protected void processSentences(SentenceIterator sentenceIter,
			String model, int times) {
		while (times-- > 0) {
			sentenceIter.reset();
			while (sentenceIter.hasNext()) {
				String sentence = sentenceIter.nextSentence();
				if (sentence == null)
					continue;

				List<VocabWord> sentenceWordList = new ArrayList<>();
				Tokenizer tokenizer = tokenizerFactory.create(sentence);
				while (tokenizer.hasMoreTokens()) {
					String token = tokenizer.nextToken();
					VocabWord word = vocab.get(token);
					if (word != null) {
						sentenceWordList.add(word);
					}
				}

				if (model == "cbow") {
					cbow(sentenceWordList);
				} else if (model == "sg") {
					skipGram(sentenceWordList);
				} else {
					System.out.println("Model name should be 'cbow' or 'sg'");
				}
			}
		}
	}

	protected void skipGram(List<VocabWord> sentenceWordList) {
		for (int i = 0; i < sentenceWordList.size(); ++i) {
			VocabWord w1 = sentenceWordList.get(i);
			int reducedWindowSize = Random.nextInt(windowSize);
			int windowBegin = Math.max(0, i - windowSize + reducedWindowSize);
			int windowEnd = Math.min(i + windowSize - reducedWindowSize,
					sentenceWordList.size());

			for (int j = windowBegin; j < windowEnd; ++j) {
				if (i != j) {
					VocabWord w2 = sentenceWordList.get(j);
					DoubleMatrix l1 = syn0.getRow(w2.getIndex());
					DoubleMatrix l2a = syn1.getRows(w1.getPoints());
					DoubleMatrix fa = MatrixUtil.sigmoid(MatrixUtil.dot(l1,
							l2a.transpose()));
					// ga = (1 - word.code - fa) * alpha # vector of error
					// gradients multiplied by the learning rate
					DoubleMatrix ga = DoubleMatrix.ones(fa.length)
							.sub(MatrixUtil.toMatrix(w1.getCodes())).sub(fa)
							.mul(alpha);
					DoubleMatrix outer = ga.mmul(l1);
					for (int k = 0; k < w1.getPoints().length; k++) {
						DoubleMatrix toAdd = l2a.getRow(k).add(outer.getRow(k));
						syn1.putRow(w1.getPoints()[k], toAdd);
					}

					DoubleMatrix updatedInput = l1.add(MatrixUtil.dot(ga, l2a));
					syn0.putRow(w2.getIndex(), updatedInput);
				}
			}
		}
	}

	protected void cbow(List<VocabWord> sentenceWordList) {
		for (int i = 0; i < sentenceWordList.size(); ++i) {
			VocabWord w1 = sentenceWordList.get(i);
			int reducedWindowSize = Random.nextInt(windowSize);
			int windowBegin = Math.max(0, i - windowSize + reducedWindowSize);
			int windowEnd = Math.min(i + windowSize - reducedWindowSize,
					sentenceWordList.size());
			DoubleMatrix l1 = DoubleMatrix.zeros(layerSize).transpose();

			int count = 0;
			for (int j = windowBegin; j < windowEnd; ++j) {
				if (i != j) {
					l1 = l1.add(syn0.getRow(sentenceWordList.get(j).getIndex()));
					++count;
				}
			}
			if (count == 0)
				continue;
			l1 = l1.div(count);

			DoubleMatrix l2a = syn1.getRows(w1.getPoints());
			DoubleMatrix fa = MatrixUtil.sigmoid(MatrixUtil.dot(l1,
					l2a.transpose()));
			// ga = (1 - word.code - fa) * alpha # vector of error
			// gradients multiplied by the learning rate
			DoubleMatrix ga = DoubleMatrix.ones(fa.length)
					.sub(MatrixUtil.toMatrix(w1.getCodes())).sub(fa).mul(alpha);

			DoubleMatrix outer = ga.mmul(l1);
			for (int k = 0; k < w1.getPoints().length; k++) {
				DoubleMatrix toAdd = l2a.getRow(k).add(outer.getRow(k));
				syn1.putRow(w1.getPoints()[k], toAdd);
			}

			DoubleMatrix updatedInput = l1.add(MatrixUtil.dot(ga, l2a));
			for (int j = windowBegin; j < windowEnd; ++j) {
				if (i != j) {
					syn0.putRow(sentenceWordList.get(j).getIndex(),
							updatedInput);
				}
			}
		}
	}

	public Word2VecSingleThread() {
		this(5, 5, 50, 0.025);
	}

	public Word2VecSingleThread(int minWordFrequency, int windowSize,
			int layerSize, double alpha) {
		this.minWordFrequency = minWordFrequency;
		this.windowSize = windowSize;
		this.layerSize = layerSize;
		this.alpha = alpha;

		Random.seed(System.currentTimeMillis());
	}

	public double[] getWordVector(String word) {
		if (vocab.containsKey(word)) {
			return syn0.getRow(vocab.get(word).getIndex()).toArray();
		}
		return null;
	}

	protected void buildVocab(SentenceIterator sentenceIter) {
		sentenceIter.reset();
		while (sentenceIter.hasNext()) {
			String sentence = sentenceIter.nextSentence();
			if (sentence == null)
				continue;

			Tokenizer tokenizer = tokenizerFactory.create(sentence);
			while (tokenizer.hasMoreTokens()) {
				String token = tokenizer.nextToken();
				if (stopWords.contains(token)) {
					continue;
				}
				if (vocab.containsKey(token)) {
					vocab.get(token).setWordFrequency(
							vocab.get(token).getWordFrequency() + 1);
				} else {
					VocabWord word = new VocabWord(1, layerSize);
					word.setIndex(vocab.size());
					vocab.put(token, word);
				}
			}
		}
	}

	protected void buildHuffmanTree() {
		PriorityQueue<VocabWord> heap = new PriorityQueue<VocabWord>(
				vocab.values());
		int i = 0;
		while (heap.size() > 1) {
			VocabWord min1 = heap.poll();
			VocabWord min2 = heap.poll();

			VocabWord add = new VocabWord(min1.getWordFrequency()
					+ min2.getWordFrequency(), layerSize);
			int index = (vocab.size() + i);

			add.setIndex(index);
			add.setLeft(min1);
			add.setRight(min2);
			min1.setCode(0);
			min2.setCode(1);
			min1.setParent(add);
			min2.setParent(add);
			heap.add(add);
			i++;
		}

		Triple<VocabWord, int[], int[]> triple = new Triple<>(heap.poll(),
				new int[] {}, new int[] {});
		Stack<Triple<VocabWord, int[], int[]>> stack = new Stack<>();
		stack.add(triple);
		while (!stack.isEmpty()) {
			triple = stack.pop();
			int[] codes = triple.getSecond();
			int[] points = triple.getThird();
			VocabWord node = triple.getFirst();
			if (node == null) {
				continue;
			}
			if (node.getIndex() < vocab.size()) {
				node.setCodes(codes);
				node.setPoints(points);
			} else {
				int[] copy = plus(points, node.getIndex() - vocab.size());
				points = copy;
				triple.setThird(points);
				stack.add(new Triple<>(node.getLeft(), plus(codes, 0), points));
				stack.add(new Triple<>(node.getRight(), plus(codes, 1), points));
			}
		}
	}

	private int[] plus(int[] addTo, int add) {
		int[] copy = new int[addTo.length + 1];
		for (int c = 0; c < addTo.length; c++)
			copy[c] = addTo[c];
		copy[addTo.length] = add;
		return copy;
	}

	protected void resetLayers() {
		syn1 = DoubleMatrix.zeros(vocab.size(), layerSize);
		syn0 = DoubleMatrix.zeros(vocab.size(), layerSize);
		for (int i = 0; i < syn0.rows; i++)
			for (int j = 0; j < syn0.columns; j++) {
				syn0.put(i, j, (double) (Random.nextDouble() - 0.5) / layerSize);
			}
	}
}
