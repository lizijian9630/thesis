package zju.dcd.zijian.thesis.deeplearning.core.sent2vec;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map.Entry;

import org.deeplearning4j.util.MatrixUtil;
import org.deeplearning4j.word2vec.VocabWord;
import org.deeplearning4j.word2vec.inputsanitation.InputHomogenization;
import org.deeplearning4j.word2vec.sentenceiterator.CollectionSentenceIterator;
import org.deeplearning4j.word2vec.sentenceiterator.SentenceIterator;
import org.deeplearning4j.word2vec.sentenceiterator.SentencePreProcessor;
import org.deeplearning4j.word2vec.tokenizer.Tokenizer;
import org.jblas.DoubleMatrix;
import org.jblas.util.Random;

import zju.dcd.zijian.thesis.deeplearning.domain.SentenceVector;
import zju.dcd.zijian.thesis.domain.Sentence;

public class Sent2VecSingleThread extends Word2VecSingleThread {

	protected List<SentenceVector> sentenceVectors = new ArrayList<>();

	@Override
	public void train(Collection<String> sentences, String model, int times) {

		for (String sentence : sentences) {
			SentenceVector sv = new SentenceVector();
			sv.content = new Sentence();
			sv.content.text = new InputHomogenization(sentence).transform();
			sv.vector = DoubleMatrix.rand(layerSize).sub(0.5).div(layerSize)
					.transpose();
			sentenceVectors.add(sv);
		}

		SentenceIterator sentenceIter = new CollectionSentenceIterator(
				new SentencePreProcessor() {
					@Override
					public String preProcess(String sentence) {
						return new InputHomogenization(sentence).transform();
					}
				}, sentences);

		this.buildVocab(sentenceIter);
		this.buildHuffmanTree();
		this.resetLayers();
		this.processSentences(sentenceIter, model, times);
	}

	@Override
	protected void processSentences(SentenceIterator sentenceIter,
			String model, int times) {
		while (times-- > 0) {
			sentenceIter.reset();
			while (sentenceIter.hasNext()) {
				String sentence = sentenceIter.nextSentence();
				if (sentence == null)
					continue;

				List<VocabWord> sentenceWordList = new ArrayList<>();
				Tokenizer tokenizer = tokenizerFactory.create(sentence);
				while (tokenizer.hasMoreTokens()) {
					String token = tokenizer.nextToken();
					VocabWord word = vocab.get(token);
					if (word != null) {
						sentenceWordList.add(word);
					}
				}

				for (int i = 0; i < sentenceVectors.size(); ++i) {
					if (sentenceVectors.get(i).content.text.equals(sentence)) {
						if (model == "cbow") {
							cbow(sentenceWordList, i);
						} else if (model == "sg") {
							skipGram(sentenceWordList, i);
						} else {
							System.out
									.println("Model name should be 'cbow' or 'sg'");
						}
						break;
					}
				}
			}
		}
	}

	protected void skipGram(List<VocabWord> sentenceWordList, int sentenceIndex) {
		for (int i = 0; i < sentenceWordList.size(); ++i) {
			int reducedWindowSize = Random.nextInt(windowSize);
			int windowBegin = Math.max(0, i - windowSize + reducedWindowSize);
			int windowEnd = Math.min(i + windowSize - reducedWindowSize,
					sentenceWordList.size());

			for (int j = windowBegin; j < windowEnd; ++j) {
				if (i != j) {
					VocabWord w2 = sentenceWordList.get(j);
					DoubleMatrix l1 = sentenceVectors.get(sentenceIndex).vector;
					DoubleMatrix l2a = syn1.getRows(w2.getPoints());
					DoubleMatrix fa = MatrixUtil.sigmoid(MatrixUtil.dot(l1,
							l2a.transpose()));
					// ga = (1 - word.code - fa) * alpha # vector of error
					// gradients multiplied by the learning rate
					DoubleMatrix ga = DoubleMatrix.ones(fa.length)
							.sub(MatrixUtil.toMatrix(w2.getCodes())).sub(fa)
							.mul(alpha);
					DoubleMatrix outer = ga.mmul(l1);
					for (int k = 0; k < w2.getPoints().length; k++) {
						DoubleMatrix toAdd = l2a.getRow(k).add(outer.getRow(k));
						syn1.putRow(w2.getPoints()[k], toAdd);
					}
					DoubleMatrix neu1e = MatrixUtil.dot(ga, l2a);
					sentenceVectors.get(sentenceIndex).vector = sentenceVectors
							.get(sentenceIndex).vector.add(neu1e);
				}
			}
		}
	}

	protected void cbow(List<VocabWord> sentenceWordList, int sentenceIndex) {
		for (int i = 0; i < sentenceWordList.size(); ++i) {
			VocabWord w1 = sentenceWordList.get(i);
			int reducedWindowSize = Random.nextInt(windowSize);
			int windowBegin = Math.max(0, i - windowSize + reducedWindowSize);
			int windowEnd = Math.min(i + windowSize - reducedWindowSize,
					sentenceWordList.size());
			DoubleMatrix l1 = DoubleMatrix.zeros(layerSize).transpose();

			int count = 0;
			for (int j = windowBegin; j < windowEnd; ++j) {
				if (i != j) {
					l1 = l1.add(syn0.getRow(sentenceWordList.get(j).getIndex()));
					++count;
				}
			}

			l1 = l1.add(sentenceVectors.get(sentenceIndex).vector);
			l1 = l1.div(count + 1);

			DoubleMatrix l2a = syn1.getRows(w1.getPoints());
			DoubleMatrix fa = MatrixUtil.sigmoid(MatrixUtil.dot(l1,
					l2a.transpose()));
			// ga = (1 - word.code - fa) * alpha # vector of error
			// gradients multiplied by the learning rate
			DoubleMatrix ga = DoubleMatrix.ones(fa.length)
					.sub(MatrixUtil.toMatrix(w1.getCodes())).sub(fa).mul(alpha);

			DoubleMatrix outer = ga.mmul(l1);
			for (int k = 0; k < w1.getPoints().length; k++) {
				DoubleMatrix toAdd = l2a.getRow(k).add(outer.getRow(k));
				syn1.putRow(w1.getPoints()[k], toAdd);
			}

			DoubleMatrix neu1e = MatrixUtil.dot(ga, l2a);
			DoubleMatrix updatedInput = l1.add(neu1e);
			for (int j = windowBegin; j < windowEnd; ++j) {
				if (i != j) {
					syn0.putRow(sentenceWordList.get(j).getIndex(),
							updatedInput);
				}
			}
			sentenceVectors.get(sentenceIndex).vector = sentenceVectors
					.get(sentenceIndex).vector.add(neu1e);
		}
	}

	public List<Entry<Sentence, double[]>> getSentenceVectors() {
		List<Entry<Sentence, double[]>> result = new ArrayList<>();
		for (SentenceVector sv : sentenceVectors) {
			result.add(new AbstractMap.SimpleEntry<Sentence, double[]>(
					sv.content, sv.vector.toArray()));
		}
		return result;
	}
}
