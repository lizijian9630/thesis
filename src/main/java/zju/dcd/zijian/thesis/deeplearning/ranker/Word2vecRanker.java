package zju.dcd.zijian.thesis.deeplearning.ranker;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.deeplearning4j.stopwords.StopWords;
import org.deeplearning4j.word2vec.tokenizer.DefaultTokenizerFactory;
import org.deeplearning4j.word2vec.tokenizer.Tokenizer;
import org.deeplearning4j.word2vec.tokenizer.TokenizerFactory;

import zju.dcd.zijian.thesis.deeplearning.core.sent2vec.Word2VecSingleThread;
import zju.dcd.zijian.thesis.deeplearning.utils.SentenceVectorAPClusterer;
import zju.dcd.zijian.thesis.domain.Document;
import zju.dcd.zijian.thesis.domain.Sentence;

//Use Cluster Centers of Sentences
//Use Average of Word2Vec Vectors to Represent Sentences
//Train Word2Vec model and obtain the representation at the same time
public class Word2vecRanker {
	private static final TokenizerFactory tokenizerFactory = new DefaultTokenizerFactory();
	private static final List<String> stopWords = StopWords.getStopWords();

	private Word2VecSingleThread trainModel(List<Document> docList,
			String model, int times, int windowSize, int layerSize) {
		List<String> sentenceStrings = new ArrayList<>();
		for (Document doc : docList) {
			for (Sentence sentence : doc.sentences) {
				sentenceStrings.add(sentence.text);
			}
		}

		Word2VecSingleThread vec = new Word2VecSingleThread();
		vec.windowSize = windowSize;
		vec.layerSize = layerSize;
		vec.train(sentenceStrings, model, times);

		return vec;
	}

	public List<Entry<Sentence, Double>> rankSentences(List<Document> docList,
			String model, int times) {
		return this.rankSentences(docList, model, times, 5, 50);
	}

	public List<Entry<Sentence, double[]>> getSentenceVectors(
			List<Document> docList, String model, int times, int windowSize,
			int layerSize) {
		Word2VecSingleThread wordVectorModel = trainModel(docList, model,
				times, windowSize, layerSize);
		List<Entry<Sentence, double[]>> sentenceVectors = new ArrayList<Entry<Sentence, double[]>>();

		for (Document doc : docList) {
			for (Sentence sentence : doc.sentences) {
				double[] sentenceVector = null;

				Tokenizer tokenizer = tokenizerFactory.create(sentence.text
						.toLowerCase());
				int tokenNumber = tokenizer.countTokens();
				while (tokenizer.hasMoreTokens()) {
					String token = tokenizer.nextToken();
					if (stopWords.contains(token)) {
						continue;
					}
					if (sentenceVector != null) {
						double[] wordVector = wordVectorModel
								.getWordVector(token);
						if (wordVector != null) {
							for (int i = 0; i < sentenceVector.length; ++i) {
								sentenceVector[i] += wordVector[i];
							}
						}
					} else {
						sentenceVector = wordVectorModel.getWordVector(token);
					}
				}
				if (sentenceVector != null) {
					for (int i = 0; i < sentenceVector.length; ++i) {
						sentenceVector[i] /= (double) tokenNumber;
					}
					sentenceVectors
							.add(new AbstractMap.SimpleEntry<Sentence, double[]>(
									sentence, sentenceVector));
				}
			}
		}

		return sentenceVectors;
	}

	public List<Entry<Sentence, Double>> rankSentences(List<Document> docList,
			String model, int times, int windowSize, int layerSize) {
		return SentenceVectorAPClusterer.cluster(this.getSentenceVectors(
				docList, model, times, windowSize, layerSize));
	}
}
