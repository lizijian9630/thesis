package zju.dcd.zijian.thesis.deeplearning.utils;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map.Entry;

import org.jblas.DoubleMatrix;

import zju.dcd.zijian.thesis.domain.Sentence;

public class SentenceVectorAPClusterer {

	// Return the centers of sentences clusters
	public static List<Entry<Sentence, Double>> cluster(
			List<Entry<Sentence, double[]>> sentenceVectors) {

		double dampingFactor = 0.5;
		int n = sentenceVectors.size();

		DoubleMatrix a = DoubleMatrix.zeros(n, n);
		DoubleMatrix r = DoubleMatrix.zeros(n, n);
		DoubleMatrix s = new DoubleMatrix(n, n);

		for (int i = 0; i < n; ++i) {
			for (int j = 0; j <= i; ++j) {
				s.put(i,
						j,
						-norm2(sentenceVectors.get(i).getValue(),
								sentenceVectors.get(j).getValue()));
			}
		}
		for (int i = 0; i < n; ++i) {
			for (int j = n - 1; j > i; --j) {
				s.put(i, j, s.get(j, i));
			}
		}

		int iter = 0;
		while (iter++ < 1000) {

			for (int i = 0; i < n; ++i) {
				for (int j = 0; j < n; ++j) {
					double maxValue = Double.MIN_VALUE;
					for (int k = 0; k < n; ++k) {
						if (k != j && a.get(i, k) + s.get(i, k) > maxValue) {
							maxValue = a.get(i, k) + s.get(i, k);
						}
					}
					r.put(i, j, (1 - dampingFactor) * (s.get(i, j) - maxValue)
							+ dampingFactor * r.get(i, j));
				}
			}

			for (int i = 0; i < n; ++i) {
				for (int j = 0; j < n; ++j) {
					float sumValue = 0;
					for (int k = 0; k < n; ++k) {
						if (i != k && j != k && r.get(k, j) > 0) {
							sumValue += r.get(k, j);
						}
					}
					if (i != j) {
						a.put(i,
								j,
								(1 - dampingFactor)
										* Math.min(0, r.get(j, j) + sumValue)
										+ dampingFactor * a.get(i, j));
					} else {
						a.put(i, j, (1 - dampingFactor) * sumValue
								+ dampingFactor * a.get(i, j));
					}
				}
			}
		}

		List<Entry<Sentence, Double>> result = new ArrayList<Entry<Sentence, Double>>();
		for (int i = 0; i < n; ++i) {
			result.add(new AbstractMap.SimpleEntry<Sentence, Double>(
					sentenceVectors.get(i).getKey(),
					(a.get(i, i) + r.get(i, i))));
		}

		Collections.sort(result, new Comparator<Entry<Sentence, Double>>() {
			public int compare(Entry<Sentence, Double> e1,
					Entry<Sentence, Double> e2) {
				return e1.getValue().equals(e2.getValue()) ? 0
						: e1.getValue() > e2.getValue() ? -1 : 1;
			}
		});

		return result;
	}

	private static double norm2(double[] a, double[] b) {
		double result = 0;
		for (int i = 0; i < a.length; ++i) {
			result += (a[i] - b[i]) * (a[i] - b[i]);
		}
		return result;
	}
}
