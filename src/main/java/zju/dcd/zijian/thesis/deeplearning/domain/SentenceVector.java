package zju.dcd.zijian.thesis.deeplearning.domain;

import org.jblas.DoubleMatrix;

import zju.dcd.zijian.thesis.domain.Sentence;

public class SentenceVector {
	public Sentence content;
	public DoubleMatrix vector;
}
