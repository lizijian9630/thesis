package zju.dcd.zijian.thesis.deeplearning.ranker;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import zju.dcd.zijian.thesis.deeplearning.core.sent2vec.Sent2VecSingleThread;
import zju.dcd.zijian.thesis.deeplearning.utils.SentenceVectorAPClusterer;
import zju.dcd.zijian.thesis.domain.Document;
import zju.dcd.zijian.thesis.domain.Sentence;

// Use Cluster Centers of Sentences
// Use Sent2Vec Representation to Represent Sentences
// Train Sent2Vec model and obtain the representation at the same time
public class Sent2vecRanker {

	public List<Entry<Sentence, Double>> rankSentences(List<Document> docList,
			String model, int times) {
		return this.rankSentences(docList, model, times, 5, 50);
	}

	public List<Entry<Sentence, double[]>> getSentenceVectors(
			List<Document> docList, String model, int times, int windowSize,
			int layerSize) {
		List<String> sentenceStrings = new ArrayList<>();
		for (Document doc : docList) {
			for (Sentence sentence : doc.sentences) {
				sentenceStrings.add(sentence.text);
			}
		}

		Sent2VecSingleThread vec = new Sent2VecSingleThread();
		vec.windowSize = windowSize;
		vec.layerSize = layerSize;
		vec.train(sentenceStrings, model, times);

		return vec.getSentenceVectors();
	}

	public List<Entry<Sentence, Double>> rankSentences(List<Document> docList,
			String model, int times, int windowSize, int layerSize) {

		return SentenceVectorAPClusterer.cluster(this.getSentenceVectors(
				docList, model, times, windowSize, layerSize));
	}
}
