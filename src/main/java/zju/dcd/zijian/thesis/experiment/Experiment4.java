package zju.dcd.zijian.thesis.experiment;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import org.jdom2.JDOMException;

import zju.dcd.zijian.thesis.dataset.tac.TacDocBuilder;
import zju.dcd.zijian.thesis.deeplearning.ranker.Sent2vecRanker;
import zju.dcd.zijian.thesis.deeplearning.ranker.Word2vecRanker;
import zju.dcd.zijian.thesis.deeplearning.utils.SentenceVectorExporter;
import zju.dcd.zijian.thesis.domain.Document;

// Experiments of:
// word2vec: google-pretrained, 1-times-train
//        (both cbow and skip-gram)
// sent2vec: 1-times-train
//        (both cbow and skip-gram)
// Only export the sentence vectors, not cluster the sentences
public class Experiment4 {
	private static void deleteDirectory(File path) {
		if (!path.exists())
			return;
		if (path.isFile()) {
			path.delete();
			return;
		}
		File[] files = path.listFiles();
		for (int i = 0; i < files.length; i++) {
			deleteDirectory(files[i]);
		}
		path.delete();
	}

	public static void process(int localOutputId, String localOutputPathRoot,
			String localDocumentPath, String localModelPath,
			String remoteResultPath, String remoteModelPath)
			throws JDOMException, IOException, ParseException {
		String localOutputPath = localOutputPathRoot + localOutputId;

		File outputDirectory = new File(localOutputPath);
		deleteDirectory(outputDirectory);
		outputDirectory.mkdirs();

		for (File docDirectory : new File(localDocumentPath).listFiles()) {
			String docDirectoryName = docDirectory.getName();
			List<Document> docList = TacDocBuilder.buildDocumentSet(
					localDocumentPath, docDirectoryName, "A");

			System.out.print(docDirectoryName + "-" + localOutputId + ": ");

			Sent2vecRanker sent2vecRanker2 = new Sent2vecRanker();
			SentenceVectorExporter.export(sent2vecRanker2.getSentenceVectors(
					docList, "cbow", 1, 5, 50), localOutputPath + "/"
					+ docDirectoryName + "-s2v-cbow.dat");
			System.out.print("s2v-cbow, ");
			SentenceVectorExporter
					.export(sent2vecRanker2.getSentenceVectors(docList, "sg",
							1, 5, 50), localOutputPath + "/" + docDirectoryName
							+ "-s2v-sg.dat");
			System.out.print("s2v-sg, ");

			Word2vecRanker sent2vecRanker3 = new Word2vecRanker();
			SentenceVectorExporter.export(sent2vecRanker3.getSentenceVectors(
					docList, "cbow", 1, 5, 50), localOutputPath + "/"
					+ docDirectoryName + "-w2v-cbow.dat");
			System.out.print("w2v-cbow, ");
			SentenceVectorExporter
					.export(sent2vecRanker3.getSentenceVectors(docList, "sg",
							1, 5, 50), localOutputPath + "/" + docDirectoryName
							+ "-w2v-sg.dat");
			System.out.print("w2v-sg, ");

			System.out.println("finish;");
		}
	}
}
