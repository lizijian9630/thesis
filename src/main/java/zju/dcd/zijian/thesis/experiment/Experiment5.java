package zju.dcd.zijian.thesis.experiment;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.Hashtable;
import java.util.List;
import java.util.Map.Entry;

import org.jdom2.JDOMException;

import zju.dcd.zijian.thesis.dataset.tac.TacDocBuilder;
import zju.dcd.zijian.thesis.dataset.tac.TacResultExporter;
import zju.dcd.zijian.thesis.deeplearning.ranker.Sent2vecRanker;
import zju.dcd.zijian.thesis.deeplearning.ranker.Word2vecRanker;
import zju.dcd.zijian.thesis.domain.Document;
import zju.dcd.zijian.thesis.domain.Sentence;
import zju.dcd.zijian.thesis.regulation.ranker.RegulationRanker1;
import zju.dcd.zijian.thesis.regulation.ranker.RegulationRanker2;
import zju.dcd.zijian.thesis.regulation.scorer.BaseRegulationScorer;
import zju.dcd.zijian.thesis.regulation.scorer.ConceptScorer;
import zju.dcd.zijian.thesis.regulation.scorer.EntityScorer;
import zju.dcd.zijian.thesis.regulation.scorer.KeywordScorer;
import zju.dcd.zijian.thesis.regulation.scorer.LengthScorer;
import zju.dcd.zijian.thesis.regulation.scorer.PositionScorer;
import zju.dcd.zijian.thesis.utils.TopSentenceSelector;

// Experiments of:
// Regulation1: Weighted Sum of Multi-Scorers
// Regulation2: Coverage-BaseLine
// word2vec: times: 1, 10
//			window size: 5, 10
//			layer size: 10, 20, 30, 40, 50, 100
//     	(both cbow and skip-gram)
// sent2vec: times: 1, 10
//			window size: 5, 10
//			layer size: 10, 20, 30, 40, 50, 100
//			(both cbow and skip-gram)
public class Experiment5 {
	private static final int[] layerSizeList = { 10, 20, 30, 40, 50, 100 };

	private static Hashtable<String, List<Entry<Sentence, Double>>> generateSingleResult(
			List<Document> docList) {
		Hashtable<String, List<Entry<Sentence, Double>>> singleResult = new Hashtable<String, List<Entry<Sentence, Double>>>();
		Sent2vecRanker sent2vecRanker2 = new Sent2vecRanker();
		Word2vecRanker sent2vecRanker3 = new Word2vecRanker();

		Hashtable<BaseRegulationScorer, Double> scorerList = new Hashtable<BaseRegulationScorer, Double>();
		scorerList.put(new LengthScorer(10), 1.0);
		scorerList.put(new PositionScorer(), 1.0);
		scorerList.put(new KeywordScorer(), 5.0);
		scorerList.put(new EntityScorer(), 5.0);
		scorerList.put(new ConceptScorer(), 5.0);

		RegulationRanker1 regulationRanker1 = new RegulationRanker1();
		List<Entry<Sentence, Double>> regulationRankResult = regulationRanker1
				.rankSentences(docList, scorerList);
		System.out.print("regulation, ");

		RegulationRanker2 regulationRanker2 = new RegulationRanker2();
		List<Entry<Sentence, Double>> baselineCoverageRankResult = regulationRanker2
				.rankSentences(docList);
		System.out.print("baseline coverage, ");

		singleResult.put("regulation",
				TopSentenceSelector.select(100, regulationRankResult));
		singleResult.put("coverage",
				TopSentenceSelector.select(100, baselineCoverageRankResult));

		for (int layerSize : layerSizeList) {
			List<Entry<Sentence, Double>> sent2vecRankResult2a_1_5 = sent2vecRanker2
					.rankSentences(docList, "cbow", 1, 5, layerSize);
			System.out.print("s2v-cbow-1-5-" + layerSize + ", ");
			List<Entry<Sentence, Double>> sent2vecRankResult2a_10_5 = sent2vecRanker2
					.rankSentences(docList, "cbow", 10, 5, layerSize);
			System.out.print("s2v-cbow-10-5-" + layerSize + ", ");
			List<Entry<Sentence, Double>> sent2vecRankResult2a_1_10 = sent2vecRanker2
					.rankSentences(docList, "cbow", 1, 10, layerSize);
			System.out.print("s2v-cbow-1-10-" + layerSize + ", ");
			List<Entry<Sentence, Double>> sent2vecRankResult2a_10_10 = sent2vecRanker2
					.rankSentences(docList, "cbow", 10, 10, layerSize);
			System.out.print("s2v-cbow-10-10-" + layerSize + ", ");

			List<Entry<Sentence, Double>> sent2vecRankResult2b_1_5 = sent2vecRanker2
					.rankSentences(docList, "sg", 1, 5, layerSize);
			System.out.print("s2v-sg-1-5-" + layerSize + ", ");
			List<Entry<Sentence, Double>> sent2vecRankResult2b_10_5 = sent2vecRanker2
					.rankSentences(docList, "sg", 10, 5, layerSize);
			System.out.print("s2v-sg-10-5-" + layerSize + ", ");
			List<Entry<Sentence, Double>> sent2vecRankResult2b_1_10 = sent2vecRanker2
					.rankSentences(docList, "sg", 1, 10, layerSize);
			System.out.print("s2v-sg-1-10-" + layerSize + ", ");
			List<Entry<Sentence, Double>> sent2vecRankResult2b_10_10 = sent2vecRanker2
					.rankSentences(docList, "sg", 10, 10, layerSize);
			System.out.print("s2v-sg-10-10-" + layerSize + ", ");

			singleResult.put("s2v-cbow-1-5-" + layerSize,
					TopSentenceSelector.select(100, sent2vecRankResult2a_1_5));
			singleResult.put("s2v-cbow-10-5-" + layerSize,
					TopSentenceSelector.select(100, sent2vecRankResult2a_10_5));
			singleResult.put("s2v-cbow-1-10-" + layerSize,
					TopSentenceSelector.select(100, sent2vecRankResult2a_1_10));
			singleResult
					.put("s2v-cbow-10-10-" + layerSize, TopSentenceSelector
							.select(100, sent2vecRankResult2a_10_10));

			singleResult.put("s2v-sg-1-5-" + layerSize,
					TopSentenceSelector.select(100, sent2vecRankResult2b_1_5));
			singleResult.put("s2v-sg-10-5-" + layerSize,
					TopSentenceSelector.select(100, sent2vecRankResult2b_10_5));
			singleResult.put("s2v-sg-1-10-" + layerSize,
					TopSentenceSelector.select(100, sent2vecRankResult2b_1_10));
			singleResult
					.put("s2v-sg-10-10-" + layerSize, TopSentenceSelector
							.select(100, sent2vecRankResult2b_10_10));
		}

		for (int layerSize : layerSizeList) {
			List<Entry<Sentence, Double>> sent2vecRankResult2a_1_5 = sent2vecRanker3
					.rankSentences(docList, "cbow", 1, 5, layerSize);
			System.out.print("w2v-cbow-1-5-" + layerSize + ", ");
			List<Entry<Sentence, Double>> sent2vecRankResult2a_10_5 = sent2vecRanker3
					.rankSentences(docList, "cbow", 10, 5, layerSize);
			System.out.print("w2v-cbow-10-5-" + layerSize + ", ");
			List<Entry<Sentence, Double>> sent2vecRankResult2a_1_10 = sent2vecRanker3
					.rankSentences(docList, "cbow", 1, 10, layerSize);
			System.out.print("w2v-cbow-1-10-" + layerSize + ", ");
			List<Entry<Sentence, Double>> sent2vecRankResult2a_10_10 = sent2vecRanker3
					.rankSentences(docList, "cbow", 10, 10, layerSize);
			System.out.print("w2v-cbow-10-10-" + layerSize + ", ");

			List<Entry<Sentence, Double>> sent2vecRankResult2b_1_5 = sent2vecRanker3
					.rankSentences(docList, "sg", 1, 5, layerSize);
			System.out.print("w2v-sg-1-5-" + layerSize + ", ");
			List<Entry<Sentence, Double>> sent2vecRankResult2b_10_5 = sent2vecRanker3
					.rankSentences(docList, "sg", 10, 5, layerSize);
			System.out.print("w2v-sg-10-5-" + layerSize + ", ");
			List<Entry<Sentence, Double>> sent2vecRankResult2b_1_10 = sent2vecRanker3
					.rankSentences(docList, "sg", 1, 10, layerSize);
			System.out.print("w2v-sg-1-10-" + layerSize + ", ");
			List<Entry<Sentence, Double>> sent2vecRankResult2b_10_10 = sent2vecRanker3
					.rankSentences(docList, "sg", 10, 10, layerSize);
			System.out.print("w2v-sg-10-10-" + layerSize + ", ");

			singleResult.put("w2v-cbow-1-5-" + layerSize,
					TopSentenceSelector.select(100, sent2vecRankResult2a_1_5));
			singleResult.put("w2v-cbow-10-5-" + layerSize,
					TopSentenceSelector.select(100, sent2vecRankResult2a_10_5));
			singleResult.put("w2v-cbow-1-10-" + layerSize,
					TopSentenceSelector.select(100, sent2vecRankResult2a_1_10));
			singleResult
					.put("w2v-cbow-10-10-" + layerSize, TopSentenceSelector
							.select(100, sent2vecRankResult2a_10_10));

			singleResult.put("w2v-sg-1-5-" + layerSize,
					TopSentenceSelector.select(100, sent2vecRankResult2b_1_5));
			singleResult.put("w2v-sg-10-5-" + layerSize,
					TopSentenceSelector.select(100, sent2vecRankResult2b_10_5));
			singleResult.put("w2v-sg-1-10-" + layerSize,
					TopSentenceSelector.select(100, sent2vecRankResult2b_1_10));
			singleResult
					.put("w2v-sg-10-10-" + layerSize, TopSentenceSelector
							.select(100, sent2vecRankResult2b_10_10));
		}
		return singleResult;
	}

	public static void process(int localOutputId, String localOutputPathRoot,
			String localDocumentPath, String localModelPath,
			String remoteResultPath, String remoteModelPath)
			throws JDOMException, IOException, ParseException {
		String localOutputPath = localOutputPathRoot + localOutputId;
		Hashtable<String, Hashtable<String, List<Entry<Sentence, Double>>>> result = new Hashtable<String, Hashtable<String, List<Entry<Sentence, Double>>>>();

		for (File docDirectory : new File(localDocumentPath).listFiles()) {
			String docDirectoryName = docDirectory.getName();
			List<Document> docList = TacDocBuilder.buildDocumentSet(
					localDocumentPath, docDirectoryName, "A");

			System.out.print(docDirectoryName + "-" + localOutputId + ": ");

			result.put(docDirectoryName.substring(0, 5),
					generateSingleResult(docList));
			System.out.println("finish;");
		}
		TacResultExporter.export(result, localOutputPath, localModelPath,
				remoteModelPath, remoteResultPath);
	}
}
