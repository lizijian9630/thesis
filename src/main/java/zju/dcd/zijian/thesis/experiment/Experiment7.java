package zju.dcd.zijian.thesis.experiment;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import models.*;
import utils.*;

// Experiments of:
// hLDA and shTM
// Run 250 Intervals
// alpha: 10.0, 20.0
// gamma: 0.5, 0.7
// eta: 0.2, 0.4, 0.6, 0.8
public class Experiment7 {

	private static final double[] alphaList = { 10.0, 20.0 };
	private static final double[] gammaList = { 0.5, 0.7 };
	private static final double[] etaList = { 0.2, 0.4, 0.6, 0.8 };

	public static void process(String dataPath, String outputPath, int K)
			throws IOException {
		ArrayList<ArrayList<Integer>> training = Utils.readDataDir(dataPath);
		ArrayList<String> vocab = Utils.readLines(dataPath + "/vocab.txt");
		ArrayList<String> label = Utils.readLines(dataPath + "/doclabel.txt");

		for (double alpha : alphaList) {
			for (double gamma : gammaList) {
				for (double eta : etaList) {
					StandardhLDA hLDA = new StandardhLDA();
					hLDA.displayTopicsInterval = 300;
					hLDA.initialize(training, vocab, 3, new Randoms(0));
					hLDA.estimate(250);

					FileWriter out = new FileWriter(new File(
							outputPath
									+ "/hlda_"
									+ String.format("%.0f_%.1f_%.1f", alpha,
											gamma, eta)));
					for (models.StandardhLDA.NCRPNode node : hLDA.documentLeaves) {
						out.write(node.nodeID + "\n");
					}
					out.close();

					LabelledhLDA shTM = new LabelledhLDA();
					shTM.displayTopicsInterval = 300;
					shTM.initialize(training, vocab, label, 3, new Randoms(0));
					shTM.acceptRate = 0.382;
					LabelledhLDA.K = K;
					shTM.estimate(250);

					out = new FileWriter(new File(
							outputPath
									+ "/shtm_"
									+ String.format("%.0f_%.1f_%.1f", alpha,
											gamma, eta)));
					for (models.LabelledhLDA.NCRPNode node : shTM.documentLeaves) {
						out.write(node.nodeID + "\n");
					}
					out.close();

					System.out.println(String.format(
							"Alpha: %.0f    Gamma: %.1f    Eta: %.1f", alpha,
							gamma, eta));
				}
			}
		}
	}
}
