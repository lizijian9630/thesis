package zju.dcd.zijian.thesis.experiment;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.Hashtable;
import java.util.List;
import java.util.Map.Entry;

import org.jdom2.JDOMException;

import zju.dcd.zijian.thesis.dataset.tac.TacDocBuilder;
import zju.dcd.zijian.thesis.dataset.tac.TacResultExporter;
import zju.dcd.zijian.thesis.deeplearning.ranker.Sent2vecRanker;
import zju.dcd.zijian.thesis.deeplearning.ranker.Word2vecRanker;
import zju.dcd.zijian.thesis.domain.Document;
import zju.dcd.zijian.thesis.domain.Sentence;
import zju.dcd.zijian.thesis.regulation.ranker.RegulationRanker1;
import zju.dcd.zijian.thesis.regulation.scorer.BaseRegulationScorer;
import zju.dcd.zijian.thesis.regulation.scorer.ConceptScorer;
import zju.dcd.zijian.thesis.regulation.scorer.EntityScorer;
import zju.dcd.zijian.thesis.regulation.scorer.KeywordScorer;
import zju.dcd.zijian.thesis.regulation.scorer.LengthScorer;
import zju.dcd.zijian.thesis.regulation.scorer.PositionScorer;
import zju.dcd.zijian.thesis.utils.TopSentenceSelector;

// Experiments of:
// Regulation: Weighted Sum of Multi-Scorers
// word2vec: google-pretrained, 1-times-train, 10-times-train, 
//           50-times-train, 100-times-train (both cbow and skip-gram)
// sent2vec: 1-times-train, 10-times-train, 
//           50-times-train, 100-times-train (both cbow and skip-gram)
public class Experiment1 {
	public static void process(int localOutputId, String localOutputPathRoot,
			String localDocumentPath, String localModelPath,
			String remoteResultPath, String remoteModelPath)
			throws IOException, JDOMException, ParseException {

		String localOutputPath = localOutputPathRoot + localOutputId;
		Hashtable<String, Hashtable<String, List<Entry<Sentence, Double>>>> result = new Hashtable<String, Hashtable<String, List<Entry<Sentence, Double>>>>();

		for (File docDirectory : new File(localDocumentPath).listFiles()) {
			String docDirectoryName = docDirectory.getName();
			List<Document> docList = TacDocBuilder.buildDocumentSet(
					localDocumentPath, docDirectoryName, "A");
			Hashtable<String, List<Entry<Sentence, Double>>> singleResult = new Hashtable<String, List<Entry<Sentence, Double>>>();

			Hashtable<BaseRegulationScorer, Double> scorerList = new Hashtable<BaseRegulationScorer, Double>();
			scorerList.put(new LengthScorer(10), 1.0);
			scorerList.put(new PositionScorer(), 1.0);
			scorerList.put(new KeywordScorer(), 5.0);
			scorerList.put(new EntityScorer(), 5.0);
			scorerList.put(new ConceptScorer(), 5.0);

			System.out.print(docDirectoryName + "-" + localOutputId + ": ");

			RegulationRanker1 regulationRanker1 = new RegulationRanker1();
			List<Entry<Sentence, Double>> regulationRankResult = regulationRanker1
					.rankSentences(docList, scorerList);
			System.out.print("regulation, ");

			Sent2vecRanker sent2vecRanker2 = new Sent2vecRanker();
			List<Entry<Sentence, Double>> sent2vecRankResult2a_1 = sent2vecRanker2
					.rankSentences(docList, "cbow", 1);
			System.out.print("s2v-cbow-1, ");
			List<Entry<Sentence, Double>> sent2vecRankResult2a_10 = sent2vecRanker2
					.rankSentences(docList, "cbow", 10);
			System.out.print("s2v-cbow-10, ");
			List<Entry<Sentence, Double>> sent2vecRankResult2a_50 = sent2vecRanker2
					.rankSentences(docList, "cbow", 50);
			System.out.print("s2v-cbow-50, ");
			List<Entry<Sentence, Double>> sent2vecRankResult2a_100 = sent2vecRanker2
					.rankSentences(docList, "cbow", 100);
			System.out.print("s2v-cbow-100, ");

			List<Entry<Sentence, Double>> sent2vecRankResult2b_1 = sent2vecRanker2
					.rankSentences(docList, "sg", 1);
			System.out.print("s2v-sg-1, ");
			List<Entry<Sentence, Double>> sent2vecRankResult2b_10 = sent2vecRanker2
					.rankSentences(docList, "sg", 10);
			System.out.print("s2v-sg-10, ");
			List<Entry<Sentence, Double>> sent2vecRankResult2b_50 = sent2vecRanker2
					.rankSentences(docList, "sg", 50);
			System.out.print("s2v-sg-50, ");
			List<Entry<Sentence, Double>> sent2vecRankResult2b_100 = sent2vecRanker2
					.rankSentences(docList, "sg", 100);
			System.out.print("s2v-sg-100, ");

			Word2vecRanker sent2vecRanker3 = new Word2vecRanker();
			List<Entry<Sentence, Double>> sent2vecRankResult3a_1 = sent2vecRanker3
					.rankSentences(docList, "cbow", 1);
			System.out.print("w2v-cbow-1, ");
			List<Entry<Sentence, Double>> sent2vecRankResult3a_10 = sent2vecRanker3
					.rankSentences(docList, "cbow", 10);
			System.out.print("w2v-cbow-10, ");
			List<Entry<Sentence, Double>> sent2vecRankResult3a_50 = sent2vecRanker3
					.rankSentences(docList, "cbow", 50);
			System.out.print("w2v-cbow-50, ");
			List<Entry<Sentence, Double>> sent2vecRankResult3a_100 = sent2vecRanker3
					.rankSentences(docList, "cbow", 100);
			System.out.print("w2v-cbow-100, ");

			List<Entry<Sentence, Double>> sent2vecRankResult3b_1 = sent2vecRanker3
					.rankSentences(docList, "sg", 1);
			System.out.print("w2v-sg-1, ");
			List<Entry<Sentence, Double>> sent2vecRankResult3b_10 = sent2vecRanker3
					.rankSentences(docList, "sg", 10);
			System.out.print("w2v-sg-10, ");
			List<Entry<Sentence, Double>> sent2vecRankResult3b_50 = sent2vecRanker3
					.rankSentences(docList, "sg", 50);
			System.out.print("w2v-sg-50, ");
			List<Entry<Sentence, Double>> sent2vecRankResult3b_100 = sent2vecRanker3
					.rankSentences(docList, "sg", 100);
			System.out.print("w2v-sg-100, ");

			singleResult.put("regulation",
					TopSentenceSelector.select(100, regulationRankResult));

			singleResult.put("s2v-cbow-1",
					TopSentenceSelector.select(100, sent2vecRankResult2a_1));
			singleResult.put("s2v-cbow-10",
					TopSentenceSelector.select(100, sent2vecRankResult2a_10));
			singleResult.put("s2v-cbow-50",
					TopSentenceSelector.select(100, sent2vecRankResult2a_50));
			singleResult.put("s2v-cbow-100",
					TopSentenceSelector.select(100, sent2vecRankResult2a_100));

			singleResult.put("s2v-sg-1",
					TopSentenceSelector.select(100, sent2vecRankResult2b_1));
			singleResult.put("s2v-sg-10",
					TopSentenceSelector.select(100, sent2vecRankResult2b_10));
			singleResult.put("s2v-sg-50",
					TopSentenceSelector.select(100, sent2vecRankResult2b_50));
			singleResult.put("s2v-sg-100",
					TopSentenceSelector.select(100, sent2vecRankResult2b_100));

			singleResult.put("w2v-cbow-1",
					TopSentenceSelector.select(100, sent2vecRankResult3a_1));
			singleResult.put("w2v-cbow-10",
					TopSentenceSelector.select(100, sent2vecRankResult3a_10));
			singleResult.put("w2v-cbow-50",
					TopSentenceSelector.select(100, sent2vecRankResult3a_50));
			singleResult.put("w2v-cbow-100",
					TopSentenceSelector.select(100, sent2vecRankResult3a_100));

			singleResult.put("w2v-sg-1",
					TopSentenceSelector.select(100, sent2vecRankResult3b_1));
			singleResult.put("w2v-sg-10",
					TopSentenceSelector.select(100, sent2vecRankResult3b_10));
			singleResult.put("w2v-sg-50",
					TopSentenceSelector.select(100, sent2vecRankResult3b_50));
			singleResult.put("w2v-sg-100",
					TopSentenceSelector.select(100, sent2vecRankResult3b_100));

			result.put(docDirectoryName.substring(0, 5), singleResult);
			System.out.println("finish;");
		}
		TacResultExporter.export(result, localOutputPath, localModelPath,
				remoteModelPath, remoteResultPath);
	}
}
