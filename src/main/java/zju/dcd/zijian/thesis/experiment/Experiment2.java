package zju.dcd.zijian.thesis.experiment;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.Hashtable;
import java.util.List;
import java.util.Map.Entry;

import org.jdom2.JDOMException;

import zju.dcd.zijian.thesis.dataset.tac.TacDocBuilder;
import zju.dcd.zijian.thesis.dataset.tac.TacResultExporter;
import zju.dcd.zijian.thesis.deeplearning.ranker.Sent2vecRanker;
import zju.dcd.zijian.thesis.domain.Document;
import zju.dcd.zijian.thesis.domain.Sentence;
import zju.dcd.zijian.thesis.utils.TopSentenceSelector;

// Experiments of:
// sent2vec: times: 1, 10
//			window size: 5, 10
//			layer size: 50, 80, 100, 200, 300, 500
//			(both cbow and skip-gram)
public class Experiment2 {

	private static final int[] layerSizeList = { 50, 80, 100, 200, 300, 500 };

	private static Hashtable<String, List<Entry<Sentence, Double>>> generateSingleResult(
			List<Document> docList) {
		Hashtable<String, List<Entry<Sentence, Double>>> singleResult = new Hashtable<String, List<Entry<Sentence, Double>>>();
		Sent2vecRanker sent2vecRanker2 = new Sent2vecRanker();

		for (int layerSize : layerSizeList) {
			List<Entry<Sentence, Double>> sent2vecRankResult2a_1_5 = sent2vecRanker2
					.rankSentences(docList, "cbow", 1, 5, layerSize);
			System.out.print("s2v-cbow-1-5-" + layerSize + ", ");
			List<Entry<Sentence, Double>> sent2vecRankResult2a_10_5 = sent2vecRanker2
					.rankSentences(docList, "cbow", 10, 5, layerSize);
			System.out.print("s2v-cbow-10-5-" + layerSize + ", ");
			List<Entry<Sentence, Double>> sent2vecRankResult2a_1_10 = sent2vecRanker2
					.rankSentences(docList, "cbow", 1, 10, layerSize);
			System.out.print("s2v-cbow-1-10-" + layerSize + ", ");
			List<Entry<Sentence, Double>> sent2vecRankResult2a_10_10 = sent2vecRanker2
					.rankSentences(docList, "cbow", 10, 10, layerSize);
			System.out.print("s2v-cbow-10-10-" + layerSize + ", ");

			List<Entry<Sentence, Double>> sent2vecRankResult2b_1_5 = sent2vecRanker2
					.rankSentences(docList, "sg", 1, 5, layerSize);
			System.out.print("s2v-sg-1-5-" + layerSize + ", ");
			List<Entry<Sentence, Double>> sent2vecRankResult2b_10_5 = sent2vecRanker2
					.rankSentences(docList, "sg", 10, 5, layerSize);
			System.out.print("s2v-sg-10-5-" + layerSize + ", ");
			List<Entry<Sentence, Double>> sent2vecRankResult2b_1_10 = sent2vecRanker2
					.rankSentences(docList, "sg", 1, 10, layerSize);
			System.out.print("s2v-sg-1-10-" + layerSize + ", ");
			List<Entry<Sentence, Double>> sent2vecRankResult2b_10_10 = sent2vecRanker2
					.rankSentences(docList, "sg", 10, 10, layerSize);
			System.out.print("s2v-sg-10-10-" + layerSize + ", ");

			singleResult.put("s2v-cbow-1-5-" + layerSize,
					TopSentenceSelector.select(100, sent2vecRankResult2a_1_5));
			singleResult.put("s2v-cbow-10-5-" + layerSize,
					TopSentenceSelector.select(100, sent2vecRankResult2a_10_5));
			singleResult.put("s2v-cbow-1-10-" + layerSize,
					TopSentenceSelector.select(100, sent2vecRankResult2a_1_10));
			singleResult
					.put("s2v-cbow-10-10-" + layerSize, TopSentenceSelector
							.select(100, sent2vecRankResult2a_10_10));

			singleResult.put("s2v-sg-1-5-" + layerSize,
					TopSentenceSelector.select(100, sent2vecRankResult2b_1_5));
			singleResult.put("s2v-sg-10-5-" + layerSize,
					TopSentenceSelector.select(100, sent2vecRankResult2b_10_5));
			singleResult.put("s2v-sg-1-10-" + layerSize,
					TopSentenceSelector.select(100, sent2vecRankResult2b_1_10));
			singleResult
					.put("s2v-sg-10-10-" + layerSize, TopSentenceSelector
							.select(100, sent2vecRankResult2b_10_10));
		}
		return singleResult;
	}

	public static void process(int localOutputId, String localOutputPathRoot,
			String localDocumentPath, String localModelPath,
			String remoteResultPath, String remoteModelPath)
			throws IOException, JDOMException, ParseException {

		String localOutputPath = localOutputPathRoot + localOutputId;
		Hashtable<String, Hashtable<String, List<Entry<Sentence, Double>>>> result = new Hashtable<String, Hashtable<String, List<Entry<Sentence, Double>>>>();

		for (File docDirectory : new File(localDocumentPath).listFiles()) {
			String docDirectoryName = docDirectory.getName();
			List<Document> docList = TacDocBuilder.buildDocumentSet(
					localDocumentPath, docDirectoryName, "A");

			System.out.print(docDirectoryName + "-" + localOutputId + ": ");

			result.put(docDirectoryName.substring(0, 5),
					generateSingleResult(docList));
			System.out.println("finish;");
		}
		TacResultExporter.export(result, localOutputPath, localModelPath,
				remoteModelPath, remoteResultPath);
	}
}
